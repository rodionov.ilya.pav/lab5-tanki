﻿using Interfaces.Command;

public class IoC
{
    public static T Resolve<T>(string key, params object[] args)
    {
        try
        {
            return (T)model[key](args);
        }
        catch (KeyNotFoundException)
        {
            throw new IoCException("KeyNotFound");
        }
        catch (InvalidCastException)
        {
            throw new IoCException("WrongArguments");
        }
    }

    public static Dictionary <string, Func<object[], object>> model = new Dictionary<string, Func<object[], object>>()
    {
        {
            "IoC.Setup", (args)=>
            {
                return new IoCSetupCommand((string)args[0], (Func<object[], object>)args[1]);
            }
        },
        {
            "EmptyCommand", (args)=>
            {
                return new EmptyCommand();
            }
        }
    };
    class IoCSetupCommand : ICommand
    {
        string key;
        Func<object[], object> strategy;

        public IoCSetupCommand(string key, Func<object[], object> strategy)
        {
            this.key = key;
            this.strategy = strategy;
        }

        public void Execute()
        {
            model.Add(key, strategy);
        }
    }
}
public class IoCException : Exception
{
    public IoCException(string message) : base(message) { }
}