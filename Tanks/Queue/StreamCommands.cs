﻿using ExceptionHandler;

namespace Queue
{
    public class StreamCommands
    {
        public StreamCommands ()
        {
            QueueCommands();
        }
        public static void QueueCommands()
        {
            CommandsQueue commandsQueue = IoC.Resolve<CommandsQueue>("GetToQueue");
            while (commandsQueue.Queue.Count > 0)
            {
                try
                {
                    commandsQueue = IoC.Resolve<CommandsQueue>("GetToQueue");
                    commandsQueue.Pull();
                }
                catch (Exception ex)
                {
                    ExceptionHandlerClass handler = IoC.Resolve<ExceptionHandlerClass>("GetExceptionHandler");
                    handler.Search(commandsQueue.Queue.First(), ex.Message);
                }
            }
        }
    }
}