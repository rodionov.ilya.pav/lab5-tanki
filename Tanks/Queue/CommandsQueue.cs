﻿using Interfaces.Command;

namespace Queue
{
    public class CommandsQueue
    {
        public List<ICommand> Queue = new List<ICommand>();

        public CommandsQueue(List<ICommand> Queue)
        {
            this.Queue = Queue;
        }

        public void Add(ICommand command)
        {
            Queue.Add(command);
        }

        public void Remove(ICommand command)
        {
            Queue.Remove(command);
        }

        public void Pull()
        {
            Queue.First().Execute();
            Queue.RemoveAt(0);
        }
    }

}
