﻿using Interfaces.Command;
using System.Numerics;

namespace Interfaces.Movable
{
    public interface IMovable
    {
        Vector2 Position { get; set; }
        Vector2 Velocity { get; set; }
        ICommand Injector
        { get; set; }
    }
}