﻿using Object;

namespace Interfaces.LogicTree
{
    public interface ILogicTree
    {
        void Add();
        bool Search(UObject obj);
    }
}