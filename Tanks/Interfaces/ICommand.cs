﻿namespace Interfaces.Command
{
    public interface ICommand
    {
        void Execute();
    }
    public class EmptyCommand : ICommand
    {
        public void Execute() { }
    }
}