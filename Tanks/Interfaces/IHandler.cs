using Interfaces.Command;

namespace Interfaces.Handler
{
    public interface IHandler
    {
        public Func<object[], object> Search(ICommand Command, string Exception);
        public void Add(ICommand Command, string newException, Func<object[], object> func);
    }
}