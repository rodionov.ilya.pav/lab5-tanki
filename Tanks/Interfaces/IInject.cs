﻿using Interfaces.Command;

namespace Interfaces.Inject
{
    public interface IInject
    {
        public void SetCommand(ICommand command);
        public ICommand GetCommand();
    }
    public class Injector : ICommand, IInject
    {
        private ICommand Command;
        public void SetCommand(ICommand command)
        {
            Command = command;
        }
        public ICommand GetCommand()
        {
            return Command;
        }

        public void Execute()
        {
            Command.Execute();
        }
    }
}