﻿using System.Numerics;

namespace Interfaces.StartMovable
{
    public interface IStartMovable
    {
        Vector2 Velocity { get; set; }
    }
}