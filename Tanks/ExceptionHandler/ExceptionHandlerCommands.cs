﻿using Interfaces.Command;
using Interfaces.Handler;

namespace ExceptionHandler
{
    public class AddExceptionHandlerCommand
    {
        ICommand Command;
        string newException;
        Func<object[], object> func;
        public AddExceptionHandlerCommand(ICommand Command, string newException, Func<object[], object> func)
        {
            this.func = func;
            this.Command = Command;
            this.newException = newException;
        }
        public void Execute()
        {
            ExceptionHandlerClass handler = IoC.Resolve<ExceptionHandlerClass>("GetExceptionHandler");
            handler.Add(Command, newException, func);
            IoC.Resolve<ExceptionHandlerClass>("RefreshExceptionHandler", handler);
        }
    }
}