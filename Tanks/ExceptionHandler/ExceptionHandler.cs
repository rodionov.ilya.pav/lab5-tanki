using Interfaces.Command;
using Interfaces.Handler;

namespace ExceptionHandler
{
    public class ExceptionHandlerClass: IHandler
    {
        public Dictionary<ICommand, Dictionary<string, Func<object[], object>>> LibraryExcepions = new Dictionary<ICommand, Dictionary<string, Func<object[], object>>>() {};
        public Func<object[], object> Search(ICommand Command, string Exception) // ��� ���������� (2 �������)
        {
            if (LibraryExcepions.ContainsKey(Command) && LibraryExcepions[Command].ContainsKey(Exception)) return LibraryExcepions[Command][Exception]; 
            return null;
        }
        public void Add(ICommand Command, string newException, Func<object[], object> func)
        {
            var _newException = new Dictionary<string, Func<object[], object>>() { { newException, func } };
            LibraryExcepions.Add(Command, _newException);
        }
    }
}