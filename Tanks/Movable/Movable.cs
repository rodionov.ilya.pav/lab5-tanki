﻿using Interfaces.Command;
using Interfaces.Movable;
using System.Numerics;

namespace Move.Movable
{
    public class Movable : IMovable
    {
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public ICommand Injector { get; set; }
    }
}
