﻿using Interfaces.StartMovable;
using Interfaces.Command;
using Interfaces.Movable;
using Interfaces.Inject;
using Object;
namespace Move.MovableCommands
{
    public class StartMoveCommand : ICommand // Команда для начала движения объекта
    {
        private UObject obj;
        public StartMoveCommand(UObject obj)
        {
            this.obj = obj;
        }
        public void Execute()
        {
            IMovable movable = IoC.Resolve<IMovable>("Init", obj);
            ICommand command = IoC.Resolve<ICommand>("Move", movable);
            IInject injector = IoC.Resolve<IInject>("Injector", command);
            movable.Injector = (ICommand)injector;
            IoC.Resolve<ICommand>("PutToQueue", injector);
        }
    }
    public class MoveCommand : ICommand // Команда для движения объекта один раз
    {
        private IMovable movable;
        public MoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            movable.Position += movable.Velocity;
        }
    }
    public class LoopMoveCommand : ICommand // Команда для движения объекта циклически, до команды стоп
    {
        private IMovable movable;
        public LoopMoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            movable.Position += movable.Velocity;
            ICommand command = IoC.Resolve<ICommand>("Move", movable);
            IInject injector = IoC.Resolve<IInject>("Injector", command);
            movable.Injector = (ICommand)injector;
            IoC.Resolve<ICommand>("PutToQueue", injector).Execute();
        }
    }
    public class StopMove : ICommand // Команда для остановки объекта в циклическом движении
    {
        IMovable movable;

        public StopMove(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            IInject MoveCommand = (IInject)movable.Injector;
            MoveCommand.SetCommand(IoC.Resolve<ICommand>("EmptyCommand"));
        }
    }
}