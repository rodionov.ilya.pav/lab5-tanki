﻿using Interfaces.Command;
using Object;

namespace LogicTree
{
    public class SearchLogicTreeCommand : ICommand
    {
        UObject Object1;
        UObject Object2;

        public SearchLogicTreeCommand(UObject Object1, UObject Object2)
        {
            this.Object1 = Object1;
            this.Object2 = Object2;
        }
        public void Execute()
        {
            LogicTreeClass logicTree = IoC.Resolve<LogicTreeClass>("GetLogicTree", Object1, Object2);
            if (!logicTree.Search(Object2)) return; // Если нет столкновения, конец функции
            throw new CollisionException("Collision", Object1, Object2); // Выбросить исключение, если столкновение было
        }
    }
    public class CollisionException : ArgumentException // Класс исключения для столкновения
    {
        public UObject Object1 { get; }
        public UObject Object2 { get; }
        public CollisionException(string message, UObject Object1, UObject Object2) : base(message)
        {
            this.Object1 = Object1;
            this.Object2 = Object2;
        }
    }
}