﻿using Interfaces.LogicTree;
using Object;

namespace LogicTree
{
    public class LogicTreeClass : ILogicTree
    {
        public Dictionary<int, Dictionary<int, Dictionary<int, int>>> LT = new Dictionary<int, Dictionary<int, Dictionary<int, int>>>();

        public bool Search(UObject obj)
        {
            if (LT.ContainsKey((int)obj.position.X))
            {
                if (LT[(int)obj.position.X].ContainsKey((int)obj.position.Y))
                {
                    if (LT[(int)obj.position.X][(int)obj.position.Y].ContainsKey((int)obj.velocity.X))
                    {
                        if (LT[(int)obj.position.X][(int)obj.position.Y][(int)obj.velocity.X] == (int)obj.velocity.Y)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public void Add() { }
    }
}
