﻿using System.Numerics;

namespace Object
{
    public class UObject
    {
        public Vector2 position;
        public Vector2 velocity;
        public UObject(Vector2 position, Vector2 velocity)
        {
            this.position = position;
            this.velocity = velocity;
        }
    }
}