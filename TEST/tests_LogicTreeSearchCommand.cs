﻿using System.Collections.Generic;
using Interfaces.Command;
using System.Numerics;
using LogicTree;
using System;
using Object;
using Xunit;

namespace TESTS
{
    public class tests_LogicTreeSearchCommand
    {
        [Fact]
        public void Test_LogicTreeSearchCommand()
        {
            IoC.Resolve<ICommand>("IoC.Setup", "GetLogicTree", (Func<object[], object>)((args) =>
            {
                LogicTreeClass tree = new LogicTreeClass();
                UObject object1 = (UObject)args[0];
                UObject object2 = (UObject)args[1];

                var level3 = new Dictionary<int, int>() { { (int)object1.velocity.X, (int)object1.velocity.Y } };
                var level2 = new Dictionary<int, Dictionary<int, int>>() { { (int)object1.position.Y, level3 } };

                tree.LT.Add((int)object1.position.X, level2);

                return tree;
            })).Execute();

            UObject object1 = new UObject(Vector2.Zero, Vector2.Zero);
            UObject object2 = new UObject(Vector2.Zero, Vector2.Zero);
            bool OnCollision = false;

            SearchLogicTreeCommand searchLogicTreeCommand = new SearchLogicTreeCommand(object1, object2);
            try
            {
                searchLogicTreeCommand.Execute();
            } 
            catch (CollisionException)
            {
                OnCollision = true;
            }

            Assert.True(OnCollision);
        }
    }
}
