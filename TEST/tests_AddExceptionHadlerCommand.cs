﻿using Interfaces.Command;
using ExceptionHandler;
using System;
using Xunit;
using Moq;

namespace TESTS
{
    public class tests_AddExceptionHadlerCommand
    {
        [Fact]
        public void Tests_AddExceptionHadlerCommand()
        {
            ExceptionHandlerClass handler = new ExceptionHandlerClass();
            bool IsGetExceptionHadler = false;

            IoC.Resolve<ICommand>("IoC.Setup", "GetExceptionHandler", (Func<object[], object>)((args) =>
            {
                IsGetExceptionHadler = true;
                return handler;
            })).Execute();

            IoC.Resolve<ICommand>("IoC.Setup", "RefreshExceptionHandler", (Func<object[], object>)((args) =>
            {
                handler = (ExceptionHandlerClass)args[0];
                return handler;
            })).Execute();

            Func<object[], object> collisionEx = (args) =>
            {
                throw new Exception("Object collision");
            };
            var commandMock = new Mock<ICommand>();
            AddExceptionHandlerCommand command = new AddExceptionHandlerCommand(commandMock.Object, "Collision", collisionEx);
            command.Execute();

            var exception = handler.Search(commandMock.Object, "Collision");

            Assert.True(IsGetExceptionHadler);
            Assert.Equal(collisionEx, exception);
        }
    }
}
