﻿using Interfaces.StartMovable;
using Move.MovableCommands;
using Interfaces.Command;
using Interfaces.Movable;
using Interfaces.Inject;
using System.Numerics;
using Move.Movable;
using System;
using Object;
using Xunit;
using Moq;

namespace TESTS
{
    public class tests_StartMovableCommand
    {
        [Fact]
        public void Test_StartMovableCommand()
        {
            bool InitMovable = false;
            bool InitInject = false;
            bool StartMoving = false;
            bool PutToQueue = false;

            Movable movable = new Movable();
            UObject obj = new UObject(new Vector2(0, 0), new Vector2(2, 2));

            IoC.Resolve<ICommand>("IoC.Setup", "Init", (Func<object[], object>)((args) =>
            {
                movable.Position = new Vector2(2, 3);
                movable.Velocity = ((UObject)args[0]).velocity;
                InitMovable = true;
                return movable;
            })).Execute();

            IoC.Resolve<ICommand>("IoC.Setup", "Injector", (Func<object[], object>)((args) =>
            {
                IInject injector = new Injector();
                injector.SetCommand((ICommand)args[0]);
                InitInject = true;
                return injector;
            })).Execute();
            IoC.Resolve<ICommand>("IoC.Setup", "Move", (Func<object[], object>)((args) =>
            {
                MoveCommand moveCommand = new MoveCommand((IMovable)args[0]);
                StartMoving = true;
                return moveCommand;
            })).Execute();
            IoC.Resolve<ICommand>("IoC.Setup", "PutToQueue", (Func<object[], object>)((args) =>
            {
                IInject inject = (IInject)args[0];
                PutToQueue = true;
                return (inject.GetCommand());
            })).Execute();

            new StartMoveCommand(obj).Execute();

            Assert.True(InitMovable);
            Assert.True(InitInject);
            Assert.True(StartMoving);
            Assert.True(PutToQueue);
            Assert.Equal(movable.Position, new Vector2(2, 3)); // Проверим сменилась ли позиция
        }
    }
}