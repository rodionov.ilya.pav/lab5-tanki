﻿using System.Collections.Generic;
using Move.MovableCommands;
using Interfaces.Movable;
using Interfaces.Command;
using Interfaces.Inject;
using System.Threading;
using System.Numerics;
using Move.Movable;
using System;
using Object;
using Queue;
using Xunit;
using Moq;

namespace TESTS
{
    public class tests_StreamCommands
    {
        [Fact]
        public void Test()
        {
            bool InitMovable = false;
            bool InitInject = false;
            bool StartMoving = false;
            bool PutToQueue = false;
            bool GetToQueue = false;

            Movable movable = new Movable();
            UObject obj = new UObject(new Vector2(0, 0), new Vector2(2, 2));

            List<ICommand> commands = new List<ICommand>() { new StartMoveCommand(obj) };

            CommandsQueue commandsQueue = new CommandsQueue(commands);

            IoC.Resolve<ICommand>("IoC.Setup", "GetToQueue", (Func<object[], object>)((args) =>
            {
                GetToQueue = true;
                return (commandsQueue);
            })).Execute();
            IoC.Resolve<ICommand>("IoC.Setup", "PutToQueue", (Func<object[], object>)((args) =>
            {
                IInject inject = (IInject)args[0];
                commandsQueue.Add(inject.GetCommand());
                PutToQueue = true;
                return (inject.GetCommand());
            })).Execute();

            IoC.Resolve<ICommand>("IoC.Setup", "Init", (Func<object[], object>)((args) =>
            {
                movable.Position = new Vector2(1, 3);
                movable.Velocity = ((UObject)args[0]).velocity;
                InitMovable = true;
                return movable;
            })).Execute();

            IoC.Resolve<ICommand>("IoC.Setup", "Injector", (Func<object[], object>)((args) =>
            {
                IInject injector = new Injector();
                injector.SetCommand((ICommand)args[0]);
                InitInject = true;
                return injector;
            })).Execute();
            IoC.Resolve<ICommand>("IoC.Setup", "Move", (Func<object[], object>)((args) =>
            {
                MoveCommand moveCommand = new MoveCommand((IMovable)args[0]);
                StartMoving = true;
                return moveCommand;
            })).Execute();

            StreamCommands streamCommands = new StreamCommands();

            Assert.True(GetToQueue);
            Assert.True(PutToQueue);
            Assert.True(InitMovable);
            Assert.True(InitInject);
            Assert.True(StartMoving);

            Assert.Equal(movable.Position, new Vector2(3, 5)); // Проверим сменилась ли позиция
        }
    }
}
